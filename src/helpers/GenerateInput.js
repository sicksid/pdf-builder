import React, { useState } from 'react'


export default function generateInput(defaultValue) {
  const [value, setValue] = useState(defaultValue)
  return <input className="form-control" defaultValue={value} onChange={event => setValue(event.target.value)} />
}
