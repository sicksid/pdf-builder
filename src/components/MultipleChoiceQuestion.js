import React, {useEffect, useState} from 'react'

export default function MultipleChoiceQuestion(props) {
  return (
    <div className="multiple-choice-question">
      {props.options.map((option, index) => {
        const optionId = `option-${option}` 
        return(
          <div key={`${optionId}-${index}`} className="custom-control custom-radio custom-control-inline">
            <input type="radio" id={optionId} name={`${props.section}/${props.composedIndex}`} className="custom-control-input" />
            <label className="custom-control-label" htmlFor={optionId}>{option}</label>
          </div>
        )
      })}
    </div>
  )
}