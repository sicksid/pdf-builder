import React, { useCallback } from 'react'
import { Link } from 'simple-react-router'
import { useMappedState } from 'redux-react-hook'


function Home(props) {
  const mapState = useCallback(
    state => ({
      questionnaires: state.questionnaires
    })
  )

  const { questionnaires } = useMappedState(mapState)

  return (
    <div className="card">
      <div className="card-body">
        <h1>Select a questionnaire</h1>
        <ul className="list-group list-group-flush">
          {questionnaires.map((questionnaire, index) => {
            return (
              <li className="list-group-item" key={index}>
                <Link href={`/view/${questionnaire.title}`}>{questionnaire.title}</Link>
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  questionnaires: state.questionnaires
})

const mapDispatchToProps = null

export default Home