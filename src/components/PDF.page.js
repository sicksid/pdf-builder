import React, { useCallback, useEffect, useState } from 'react'
import { Editor } from '@tinymce/tinymce-react'
import { useMappedState } from 'redux-react-hook'
import Spinner from './Spinner'
import download from 'downloadjs'
import { Link } from 'simple-react-router';

export default function PDF(props) {
  const { questionnaire_id } = props.location.params
  const [content, setContent] = useState(null)
  const [loading, setLoading] = useState(false)
  const mapState = useCallback(
    state => ({
      selected: state.selected
    }), [questionnaire_id]
  )
  const { selected } = useMappedState(mapState)
  useEffect(() => {
    if (!content) {
      fetch('/process?format=html', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(selected)
      }).then(
        response => response.text()
      ).then(
        html => {
          setContent(html)
        }
      )
    }
  }, [content])

  function status(response) {
    if (!response.ok) {
      return Promise.reject()
    }
    return response
  }

  function generatePDF() {
    setLoading(true)
    fetch(`/process?format=pdf&title=${selected.title}`, {
      method: 'POST',
      body: content
    }).then(
      status
    ).then(
      response => response.blob()
    ).then(
      pdf => download(pdf, `${selected.title}.pdf`, "application/pdf") && setLoading(false)
    ).catch(
      _ => alert("Failed processing PDF") && setLoading(false)
    )
  }

  const generatePDFButtonContent = loading ? <Spinner /> : <span>Generate PDF</span>
  return (
    <div className="card">
      <div className="card-header">
        <h1>PDF Editor</h1>
        <button className="btn btn-primary" disabled={loading} onClick={generatePDF}>
          {generatePDFButtonContent}
        </button>
        <Link className="btn btn-secondary" href={`/edit/${selected.title}`}>Edit</Link>
        <Link className="btn btn-info" href="/">Go to questionnaires</Link>
      </div>
      <div className="card-body">
        <Editor
          apiKey="3h3py02bwh7v2ltd3key4wmli49zxvox3q8qe3juw3cxhhvy"
          onEditorChange={html => setContent(html)}
          value={content} />
      </div>
    </div>
  )
}