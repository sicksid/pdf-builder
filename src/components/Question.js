import React from 'react'
import MultipleChoiceQuestion from './MultipleChoiceQuestion'
import GenerateInput from '../helpers/GenerateInput'

function getQuestionByType({ type, options }, section, composedIndex) {
  switch (type) {
    case 'number':
    case 'text':
      return null
    case 'multiple choice':
      return <MultipleChoiceQuestion options={options} section={section} composedIndex={composedIndex} editing={true} />

    default:
      return <bold>Invalid type of question</bold>
  }
}

export default function Question(props) {
  return (
    <div key={props.composedIndex}>
      <h3>{props.question.type}</h3>
      {GenerateInput(props.question.name)}
      {getQuestionByType(props.question, props.section.replace(' ', '-'), props.composedIndex)}
    </div>
  )
}