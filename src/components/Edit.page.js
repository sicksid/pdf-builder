import React, { useCallback } from 'react'
import { useMappedState } from 'redux-react-hook'
import { Link } from 'simple-react-router'
import GenerateInput from '../helpers/GenerateInput'
import Section from './Section'


function Edit(props) {
  const mapState = useCallback(
    state => ({
      selected: state.selected
    })
  )

  const { selected } = useMappedState(mapState)
  const sections = Object.keys(selected.questionnaire)
  const title = selected.title
  
  function getQuestionnaireQuestions(section) {
    const questionnaire = selected.questionnaire
    return questionnaire[section] || []
  }
  return (
    <div className="card">
      <div className="card-header">
        {GenerateInput(title)}
        <Link className="btn btn-primary" href={`/view/${title}`}>View</Link>
      </div>
      <div className="card-body">
        <div className="list-group list-group-flush">
          {sections.map((section, index) =>
            <Section
              key={index}
              index={index}
              section={section}
              questions={getQuestionnaireQuestions(section)} />
          )}
        </div>
      </div>
    </div>
  )
}

export default Edit