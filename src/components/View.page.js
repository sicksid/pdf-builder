import React, {useEffect, useCallback} from 'react'
import { Link } from 'simple-react-router'
import { selectQuestionnaire } from '../store/actions';
import MultipleChoiceQuestion from './MultipleChoiceQuestion'
import { useDispatch, useMappedState } from 'redux-react-hook';

function View(props) {
  const { questionnaire_id } = props.location.params

  const mapState = useCallback(
    state => ({
      selected: state.selected
    })
  )

  const { selected } = useMappedState(mapState)

  const dispatch = useDispatch()
  
  useEffect(() => {
    dispatch(selectQuestionnaire(questionnaire_id))
  }, [questionnaire_id])

  const sections = selected ? Object.keys(selected.questionnaire) : []
  const title = selected ? selected.title : null

  function getQuestionnaireQuestion(section) {
    const questionnaire = selected ? selected.questionnaire : {}
    return questionnaire[section] || []
  }

  function getQuestionByType({type, options}, section, composedIndex) {
    switch(type) {
      case 'number':
      case 'text':
        return <input type={type} />

      case 'multiple choice':
        return <MultipleChoiceQuestion options={options} section={section} composedIndex={composedIndex}/>
      
      default:
        return <bold>Invalid type of question</bold>
    }
  }

  return (
    <div className="card">
      <div className="card-header">
        <h1>{title}</h1>
        <Link className="btn btn-info" href={`/edit/${title}`}>Edit</Link>
        <Link className="btn btn-primary" href={`/pdf/${title}`}>Preview PDF</Link>
      </div>
      <div className="card-body">
        <ul className="list-group list-group-flush">
          {sections.map((section, index) => {
            return (
              <li className="list-group-item" key={index}>
                <h2>{section}</h2>
                <ul className="list-group list-group-flush">
                  {getQuestionnaireQuestion(section).map((question, second_index) => {
                    const composedIndex = `${index}-${second_index}`
                    return (
                      <li key={composedIndex}>
                        <h3>{question.name}</h3>
                        {getQuestionByType(question, section.replace(' ', '-'), composedIndex)}
                      </li>
                    )
                  })}
                </ul>
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  )
}


export default View