import React, { useEffect, useState } from 'react'
import Question from './Question'
import GenerateInput from '../helpers/GenerateInput'

export default function Section(props) {
  return (
    <div className="list-group-item">
      <label>Section Name</label>
      {GenerateInput(props.section)}
      <ul className="list-group list-group-flush">
        {props.questions.map((question, second_index) => {
          const composedIndex = `${props.index}-${second_index}`
          return (
            <Question
              key={composedIndex}
              index={composedIndex}
              question={question}
              section={props.section} />
          )
        })}
      </ul>
    </div>
  )
}