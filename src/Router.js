import React from 'react'
import SimpleReactRouter from 'simple-react-router'
import NotFound from './components/NotFound.page'
import Home from './components/Home.page'
import New from './components/New.page'
import Edit from './components/Edit.page'
import View from './components/View.page'
import PDF from './components/PDF.page'


export default class Router extends SimpleReactRouter {
    routes(map) {
        map('/', Home)
        map('/new', New)
        map('/edit/:questionnaire_id', Edit)
        map('/view/:questionnaire_id', View)
        map('/pdf/:questionnaire_id', PDF)
        map('/:path*', NotFound) // catchall route
    }
}