import React from 'react'
import { StoreContext } from 'redux-react-hook';
import questionnairesReducer from './store/reducers'
import { populateQuestionnaires } from './store/actions'
import { createStore } from 'redux'
import ReactDOM from 'react-dom'
import Router from './Router'

const store = createStore(questionnairesReducer)

fetch('/questionnaires').then(
  response => response.json()
).then(
  response => store.dispatch(populateQuestionnaires(response))
)

function App() {
  return (
    <div className="container">
      <Router />
    </div>
  )
}

const rootElement = document.getElementById('web-application')

ReactDOM.render(
  <StoreContext.Provider value={store}>
    <App />
  </StoreContext.Provider>,
  rootElement
)