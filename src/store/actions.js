export const SELECT = 'SELECT';
export const POPULATE = 'POPULATE';

export const selectQuestionnaire = questionaireId => {
  return {
    type: SELECT,
    payload: questionaireId
  }

}

export const populateQuestionnaires = questionnaires => {
  return {
    type: POPULATE,
    payload: questionnaires
  }
}