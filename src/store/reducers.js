import {
  SELECT,
  POPULATE
} from './actions';

const initialState = {
  questionnaires: [],
  selected: null
}

export default function questionnairesReducer(state = initialState, action) {
  let questionnaires
  switch (action.type) {
    case SELECT:
      questionnaires = state.questionnaires
      return {
        ...state,
        selected: questionnaires.find(questionnaire => questionnaire.title == action.payload)
      };
    case POPULATE:
      return {
        ...state,
        questionnaires: action.payload
      }
    default:
      return state;
  }
}