    
require 'sinatra'
require 'haml'
require 'sass/plugin/rack'
require './server'

Sass::Plugin.options[:style] = :compressed
use Sass::Plugin::Rack

Server.run!