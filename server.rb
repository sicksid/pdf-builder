require 'sinatra/base'
require 'json'
require 'byebug'
require "sinatra/reloader"

class Server < Sinatra::Base

  configure do
    set :server, :puma
    set :static, true
    set :public_folder, Proc.new { File.join(root, "static") }
    set :views, Proc.new { File.join(root, "templates") }
    set :port, 5000
    register Sinatra::Reloader
  end


  get '/' do
    haml :index
  end

  get '/questionnaires' do
    content_type :json

    directories = Dir.entries('storage/questionnaires').select {|file| !File.directory?(file) && file != '.gitkeep'}

    data = directories.map do |directory|
      file_path = File.join(File.dirname(__FILE__), 'storage/questionnaires', directory)
      {
        "title": directory.sub!('.json', ''),
        "questionnaire": JSON.parse(File.read(file_path))
      }
    end

    data.to_json
  end

  get '/questionnaires/:id' do
    content_type :json

    directories = Dir.entries('storage/questionnaires').select {|file| !File.directory?(file) && file != '.gitkeep' && file.include?(params[:id])}
    data = directories.map do |directory|
      file_path = File.join(File.dirname(__FILE__), 'storage/questionnaires', directory)
      {
        "title": directory.sub!('.json', ''),
        "questionnaire": JSON.parse(File.read(file_path))
      }
    end

    data.first.to_json
  end

  post '/process' do
    content_type :html
    format = params["format"]
    if format == "html"
      data = JSON.parse(request.body.read)
      pdf_path = File.join(File.dirname(__FILE__), 'pdf', 'template.haml')
      return Haml::Engine.new(File.read(pdf_path)).render(Object.new, data)
    elsif format == "pdf"
      title = params["title"]
      pdf_path = "storage/documents/#{title}.pdf"
      exitcode = system "echo \"#{request.body.read}\" | wkhtmltopdf --print-media-type - #{pdf_path}"
      if exitcode
        return send_file File.join(File.dirname(__FILE__), pdf_path)
      else
        status 500
      end
    end
  end

  not_found do
    status 302
    redirect '/'
  end
end